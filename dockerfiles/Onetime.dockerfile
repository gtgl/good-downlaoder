FROM python:3.7

# Install ffmpeg
RUN export DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install --no-install-recommends ffmpeg
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Install pip dependency
COPY requirements.txt /
RUN pip3 install --no-cache-dir -r requirements.txt

# Copy main program
COPY main.py /

# Execute set limit to one for testing
CMD ["python", "main.py", "1"]
