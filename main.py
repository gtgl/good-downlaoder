import requests
import json
from bs4 import BeautifulSoup
import logging
import random
import subprocess
import os
import sys
import shutil

VIDEO = 1
AUDIO = 3

logger = logging.getLogger(__name__)
fh = logging.FileHandler('out.log')
fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(fmt)
sh = logging.StreamHandler()
sh.setFormatter(fmt)
logger.addHandler(fh)
logger.addHandler(sh)

logger.setLevel(logging.DEBUG)


def retrieveIDs(restrict=None):
    logger.info(f'Retrieving account list...')
    accountID = []
    page = 1
    listUrl = 'https://api.swag.live/feeds/happy-hour-zh?limit=30&page='
    r = requests.get(listUrl+str(page))
    while r.status_code == 200:
        freeList = r.json()
        accountID += [x['id'] for x in freeList]
        page += 3
        r = requests.get(listUrl+str(page))
    logger.info(f'All accounts retrieved, total num: {len(accountID)}')
    if restrict:
        logger.debug(f'Detect limit, reduced length to {restrict}')
        return accountID[:restrict]
    return accountID


def writeFile(name, array):
    with open(name, 'w+b') as f:
        for b in array:
            f.write(b)


def getFile(accountID, contentType):
    binary = []
    logger.info(f'Processing ID: {accountID} contentType: {contentType}')
    for i in range(random.randint(9, 13)):
        if i == 0:
            fileType = 'mp4'
        else:
            fileType = 'm4s'
        videoUrl = f'https://asia.messages.swag.live/{accountID}-{contentType}-{str(i).zfill(5)}.{fileType}'
        logger.debug(f'GET {videoUrl}')
        # https://asia.messages.swag.live/5e8665dade682a0949c3e5a6-1-00000.mp4
        r = requests.get(videoUrl)
        binary.append(r.content)
    return binary


def combineVideoAudio(outputName, videoFile, audioFile):
    logger.info(f'Combine file {videoFile}+{audioFile}={outputName}')
    # ffmpeg.exe -y -i tmp\5eb70dbc094b74a8cee37b74_a.mp4 -i tmp\5eb70dbc094b74a8cee37b74_v.mp4 -c:a aac -c:v copy output.mp4
    cmd = f'ffmpeg -y -i {audioFile} -i {videoFile} -c:a aac -c:v copy {outputName}'
    logger.debug(f'command {cmd}')
    return subprocess.call(cmd, shell=True)


def downloadVideo(accountID):
    if not os.path.exists('tmp'):
        logger.debug('No tmp folder found, creating')
        os.mkdir('tmp')
        logger.debug('tmp created.')

    logger.info(f'Processing account: {accountID}')
    videoFile = f'tmp/{accountID}_v.mp4'
    audioFile = f'tmp/{accountID}_a.mp4'

    videoBinary = getFile(accountID, VIDEO)
    audioBinary = getFile(accountID, AUDIO)

    writeFile(videoFile, videoBinary)
    writeFile(audioFile, audioBinary)

    r = combineVideoAudio(f'out/{accountID}.mp4', videoFile, audioFile)
    if r != 0:
        logger.critical(f'File combine error at {accountID}')


def collectVideo(ids, restrict=None):
    if len(ids) == 0 and not restrict:
        accounts = retrieveIDs()
    elif restrict:
        accounts = retrieveIDs(restrict=restrict)
    else:
        accounts = ids

    for uid in accounts:
        downloadVideo(uid)

    # Clear tmp
    if os.path.exists('tmp'):
        shutil.rmtree('tmp')
        os.makedirs('tmp')


if __name__ == "__main__":
    if len(sys.argv) > 1:
        limit = int(sys.argv[1])
        collectVideo([], limit)
    else:
        collectVideo([])
